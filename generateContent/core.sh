#!/bin/sh
redColor='\033[0;31m'
greenColor='\033[0;32m'
orangeColor='\033[0;33m'
purpleColor='\033[0;35m'
cyanColor='\033[0;36m'
resetColor='\033[0m'

echoMessage() {
  message="${1}"
  typeOfMessage="${2}"
  seeMessage="false"
  colorMessage="${resetColor}"

  case "${typeOfMessage}" in 
    "--notify")
      seeMessage="true"
      colorMessage="${greenColor}"
    ;;
    "--advise")
      seeMessage="true"
      colorMessage="${orangeColor}"
    ;;
    "--error")
      seeMessage="true"
      colorMessage="${redColor}"
    ;;
    *)
      seeMessage="false"
      colorMessage="${resetColor}"
    ;;
  esac 

  if [ "${seeMessage}" = "true" ] || [ "${comments}" = "true" ]
    then
      echo "${colorMessage}${message}${resetColor}"
  fi
}
