#!/bin/sh
comments="${1}"

initialFolder=$PWD
generator="${initialFolder}/generateContent"
. ${generator}/deal.sh

book="${initialFolder}/${novelNoun}"
web="${initialFolder}/${webNoun}"

hugoContent="${web}/${webContent}"
mkdir -p ${hugoContent}


echoMessage "· Updating static files..." --notify

staticFile="autors.md"
cp -a ${book}/${staticFile} -t ${hugoContent} --remove-destination && echoMessage "  '${staticFile}' file updated." --advise || echoMessage "  Error updating '${book}/${staticFile}'!" --error

staticFolder="comic"
cp -r ${book}/${staticFolder}  -t ${hugoContent} --remove-destination && echoMessage "  '${staticFolder}' folder updated." --advise || echoMessage "  Error updating folder '${book}/${staticFolder}'!" --error


configFile="config.toml"
echo "
[[menu.main]]
name = \"(··)\"
url = \"autors\"
weight = 999
" >> "${web}/${configFile}" && echoMessage "  '${configFile}' config file updated." --advise || echoMessage "  Error updating '${web}/${configFile}'!" --error

echoMessage "  Static files updated." --notify
