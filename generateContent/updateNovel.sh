#!/bin/sh
historyWeight="${1}"
historyName="${2}"
historyTitle="${3}"
comments="${4}"
disableConventions="${5}"

initialFolder=$PWD
generator="${initialFolder}/generateContent"
. ${generator}/core.sh
. ${generator}/main.sh
. ${generator}/dictionary.sh
. ${generator}/deal.sh

book="${initialFolder}/${novelNoun}/${historyName}"

echoMessage "· Creating '${book}'..." --notify

if [ ! -d ${book} ]
  then
    echoMessage "  Book '${historyName}' not exist!" --error
    exit
fi



web="${initialFolder}/${webNoun}"

configFile="config.toml"
echo "
[[menu.main]]
name = \"${historyTitle}\"
url = \"${historyName}\"
weight = ${historyWeight}
" >> "${web}/${configFile}" && echoMessage "  Book '${historyName}': Hugo config file updated (${configFile})." --advise || echoMessage "  Book '${historyName}': Error in Hugo config file update (${web}/${configFile})!" --error

echo "[${historyTitle}](${historyName})\n" >> "${web}/${webContent}/_index.md"



hugoIndex="_index.md"

partFilename="${partNoun}"
chapterFilename="${chapterNoun}"
episodeFilename="${episodeNoun}"
sceneFilename="${sceneNoun}"

hugoContent="${web}/${webContent}/${historyName}"

mkdir -p ${hugoContent}

cd $book

hugoBook="${hugoContent}/${hugoIndex}"

newContainer "${hugoBook}"
fromFile ${hugoIndex}
addFileToContainer
content "### ${table}\n"
addContentToContainer


echoMessage "  Reading list of '${partNoun}'."

allParts=$(ls -d ${partFilename}-*/ | cut -f1 -d'/')

for part in ${allParts}
  do

    partNumber=$(splitByLastDash "${part}")
    linkPart="${partFilename}-${partNumber}"

    echoMessage "  Changing to directory: '${part}'."
    cd ${part}
    subtitleOnPartIndex=$(cat ${hugoIndex} | grep -oP '^subtitle.*"\K[^"]+' | head -1)

    hugoPart="${hugoContent}/${part}"
    mkdir -p "${hugoPart}"

    partIndex="${hugoPart}/${hugoIndex}"

    newContainer "${partIndex}"
    fromFile ${hugoIndex}
    addFileToContainer
    content "#### ${table}\n"
    addContentToContainer

    echoMessage "  Collecting all '${chapterNoun}'."

    allChapters=$(ls -d ${chapterFilename}-*)

    for chapter in ${allChapters}
      do

        chapterNumber=$(splitByLastDash "${chapter}")
        linkChapter="${chapterFilename}-${chapterNumber}"

        cd ${chapter}
        subtitleOnChapterIndex=$(cat ${hugoIndex} | grep -oP '^subtitle.*"\K[^"]+' | head -1)

        hugoChapter="${hugoPart}/${chapter}"
        mkdir -p ${hugoChapter}

        chapterIndex="${hugoChapter}/${hugoIndex}"

        newContainer "${chapterIndex}"
        fromFile ${hugoIndex}
        addFileToContainer

        echoMessage "  Reading first '${episodeNoun}'."

        labelFirstChapterNumber="${titleChapter} ${chapterNumber}: "
        if [ "${disableConventions}" = "${labelDisableConventions}" ]
          then 
            labelFirstChapterNumber=""
        fi

        firstEpisode=$(ls -d ${episodeFilename}-* | head -n 1)
        content "[${labelFirstChapterNumber}${titleStartReading}](${firstEpisode})\n"
        addContentToContainer

        echoMessage "  Collecting all '${episodeNoun}'."

        allEpisodes=$(ls -d ${episodeFilename}-*)

        for episode in ${allEpisodes}
          do

            episodeNumber=$(splitByLastDash "${episode}")

            echoMessage "  Changing to directory: '${episode}'."

            cd ${episode}

            echoMessage "  Collecting all '${sceneNoun}'."

            allScenes=$(ls -d ${sceneFilename}-*)

            hugoEpisode="${hugoChapter}/${episode}.md"

            newContainer "${hugoEpisode}"
            fromFile "${hugoIndex}"
            addFileToContainer

            for scene in ${allScenes}
              do
                fromFile "${scene}"
                addFileToContainer
            done
            cd ..

        done
        cd ..

        labelChapterNumber="${titleChapter} ${chapterNumber}: "
        if [ "${disableConventions}" = "${labelDisableConventions}" ]
          then 
            labelChapterNumber=""
        fi

        container "${partIndex}"
        content "\n[${labelChapterNumber}${subtitleOnChapterIndex}](${linkChapter}/)\n"
        addContentToContainer

    done
    cd ..

    labelPartNumber="${titlePart} ${partNumber}: "
    if [ "${disableConventions}" = "${labelDisableConventions}" ]
      then 
        labelPartNumber=""
    fi

    container "${hugoBook}"
    content "\n[${labelPartNumber}${subtitleOnPartIndex}](${linkPart}/)\n"
    addContentToContainer

done
cd ..

cd $initialFolder



echoMessage "  Book '${historyName}': Content updated." --notify
