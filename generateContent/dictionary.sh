#!/bin/sh

export titleStartReading="Empezar a leer por el principio"

export table="Índice"
export titlePart="Parte"
export titleChapter="Capítulo"
export titleEpisode="Episodio"
export titleSinopsis="Sinopsis"

export titleFirstEpisode="Primer episodio"
export titleLastEpisode="Último episodio"
export nextEpisode="En el siguiente episodio..."

export directLinks="Enlaces directos"
