#!/bin/sh

initialFolder=$PWD
generator="${initialFolder}/generateContent"
. ${generator}/core.sh

export labelDisableConventions="--disableConventions"

export webNoun="web"
export webContent="content"

export novelNoun="novela"
export partNoun="parte"
export chapterNoun="capitulo"
export episodeNoun="episodio"
export sceneNoun="escena"

export sinopsisNoun="sinopsis"
export nextChapterNoun="next"

echoMessage "· Preparing project '${novelNoun}'..."

if [ ! -d ${webNoun} ]
  then
    mkdir -p ${webNoun} && echoMessage "  Creating '${webNoun}' project." --advise || echoMessage "  Error creating '${webNoun}' project!" --error
fi

if [ ! -d "${webNoun}/${webContent}" ]
  then
    mkdir -p "${webNoun}/${webContent}" && echoMessage "  Creating '${webNoun}/${webContent}' structure." --advise || echoMessage "  Error creating '${webNoun}/${webContent}' structure!" --error
fi

if [ ! -d ${novelNoun} ]
  then
    echoMessage "  Project '${novelNoun}' does not exist!" --error
  else
    echoMessage "  Project '${novelNoun}' prepared."
fi
