#!/bin/sh
historyWeight="${1}"
historyName="${2}"
historyTitle="${3}"
comments="${4}"
disableConventions="${5}"

initialFolder=$PWD
generator="${initialFolder}/generateContent"
. ${generator}/core.sh
. ${generator}/dictionary.sh
. ${generator}/deal.sh

book="${initialFolder}/${novelNoun}/${historyName}"

if [ ! -d ${book} ]
  then
    echoMessage "  Book '${historyName}' not exist!" --error
    exit
fi

echoMessage "· Creating '${book}' sinopsis..." --notify

web="${initialFolder}/${webNoun}"



if [ "${disableConventions}" != "${labelDisableConventions}" ]
  then

    hugoContent="${web}/${webContent}/${historyName}"
    mkdir -p "${hugoContent}"

    echoMessage "  Adding content '${hugoContent}'."

    sliceFilename="${partNoun}-"
    if [ "${disableConventions}" = "${labelDisableConventions}" ]
      then 
        sliceFilename=""
    fi

    sinopsis="${hugoContent}/${sinopsisNoun}.md"

    allSinopsisContent=$(ls ${book}/${sliceFilename}*/${chapterNoun}-*/${episodeNoun}-*/${sinopsisNoun}.md)

    if [ -f ${sinopsis} ]
      then
        rm ${sinopsis}
    fi

    cat "${book}/_date.md" >> $sinopsis

    sinopsisTitle=$(echo "${sinopsisNoun}" | head -c 1 | tr '[:lower:]' '[:upper:]' && echo "${sinopsisNoun}" | cut -c 2- )
    echo "${sinopsisTitle}: " >> $sinopsis

    echoMessage "  Adding content '${sinopsisNoun}'."

    for file in ${allSinopsisContent}
      do
        cat $file >> $sinopsis
        printf "\n" >> $sinopsis

        lastSinopsis=$(cat $file)
    done

    echoMessage "  Adding content '${directLinks}'."
    initialIndex="${hugoContent}/_index.md"

    echo "\n- - -\n" >> ${initialIndex}
    echo "***${directLinks}***\n" >> ${initialIndex}

    remove="${hugoContent}/"
    theSinopsis=$(echo $sinopsis | sed "s#${remove}\(.*\).md#\1#g")

    echo "[${titleSinopsis}](${theSinopsis})\n" >> ${initialIndex}

    listFirstEpisode=$(ls ${hugoContent}/${sliceFilename}*/${chapterNoun}-*/${episodeNoun}-* | head -n 1)

    remove="${hugoContent}/"
    firstEpisode=$(echo $listFirstEpisode | sed "s#${remove}\(.*\).md#\1#g")

    echo "[${titleFirstEpisode}](${firstEpisode})\n" >> "${hugoContent}/_index.md"

    allEpisodeContent=$(ls ${hugoContent}/${sliceFilename}*/${chapterNoun}-*/${episodeNoun}-*)
    for file in ${allEpisodeContent}
      do
        remove="${hugoContent}/"
        lastEpisode=$(echo $file | sed "s#${remove}\(.*\).md#\1#g")
    done

    echo "[${titleLastEpisode}](${lastEpisode})\n" >> "${hugoContent}/_index.md"

    if [ -f "${book}/${nextChapterNoun}.md" ]
      then
        echo "***${nextEpisode}***\n" >> ${initialIndex}
        cat "${book}/${nextChapterNoun}.md" >> $initialIndex

    fi

fi


echoMessage "  Book '${historyName}': Sinopsis file updated." --notify
