#!/bin/sh

splitByLastDash() {
  string="$1"
  IFS='-' read -r title number <<EOF
  $string
EOF
  echo "$number"
}



content() {
  content="${1}"
}

container() {
  container="${1}"
}

fromFile() {
  fromFile="${1}"
}

newLine() {
  printf "\n" >> ${container}
}



addFileToContainer() {
  cat ${fromFile} >> ${container}
  newLine
}

addContentToContainer() {
  echo -n ${content} >> ${container}
  newLine
}

newContainer() {
  container "${1}"
  if [ -f ${container} ]
    then rm ${container}
  fi
  touch ${container}
}

