#!/bin/sh
comments="${1}"

initialFolder=$PWD
generator="${initialFolder}/generateContent"
. ${generator}/deal.sh

book="${initialFolder}/${novelNoun}"
web="${initialFolder}/${webNoun}"
hugoContent="${web}/${webContent}"

echoMessage "· Cleaning Hugo content..." --notify

cleanHugoContent=$(if [ -d ${hugoContent} ] ; then rm -r ${hugoContent} ; fi)

mkdir -p ${hugoContent}
echoMessage "  Hugo content cleaned." --advise

staticFile="config.toml"
cp -a ${book}/${staticFile} -t ${web} --remove-destination && echoMessage "  Created Hugo config file (${web}/${staticFile}) of project." --notify || echoMessage "  Error creating Hugo config file (${book}/${staticFile}) of project!" --error
