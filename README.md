#A diversis fabula

##Subrepositorios

- comic
- draft
- novela
- web


##Creación de cómic

Se trabaja sobre 'comic', se genera un pdf/png y se distribuye.


##Redactar nuevo contenido

Trabajamos sobre 'draft' y cuando tenemos terminado el texto lo copiamos a 'novela' siguiendo la estructura establecida.


##Actualizar la web en local

sh ./updateContent.sh

El origen de la novela se recoge de 'novela' y se genera en 'web'.


##Visualizar la web en local

Necesitas tener Hugo instalado.

Posteriormente, desde la carpeta web:

~~~
hugo server --config config-localhost.toml
~~~

La visualización está en: http://localhost:1313/

(El archivo config.toml tiene la dirección para gitlab-ci)

Una vez revisado se suben los cambios al repositorio y estos se publican directamente en adiversisfabula.gitlab.io
