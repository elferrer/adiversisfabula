#!/bin/sh
option="${1}"

if [ "${option}" = "--help" ]
  then 
    echo "Available options:"
    echo "--help: This help."
    echo "--verbose: See more messages."
    return 
fi

comments="false"
if [ "${1}" = "--verbose" ]
  then
    comments="true"
fi

sh ./generateContent/cleanHugoContent.sh "${comments}"

sh ./generateContent/updateNovel.sh "1" "preliminares" "Preliminares" "${comments}" --disableConventions
sh ./generateContent/updateSinopsis.sh "1" "preliminares" "Preliminares" "${comments}" --disableConventions

sh ./generateContent/updateNovel.sh "2" "ciclos" "Ciclos" "${comments}"
sh ./generateContent/updateSinopsis.sh "2" "ciclos" "Ciclos" "${comments}"

sh ./generateContent/updateNovel.sh "3" "ajysyt" "Ajysyt" "${comments}"
sh ./generateContent/updateSinopsis.sh "3" "ajysyt" "Ajysyt" "${comments}"

sh ./generateContent/updateStaticFiles.sh "${comments}"
